function validateAndProceed() {
    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const phone = document.getElementById("phone").value;
  
    const nameError = document.getElementById("name-error");
    const emailError = document.getElementById("email-error");
    const phoneError = document.getElementById("phone-error");
  
    nameError.textContent = "";
    document.getElementById("name").style.border = "1px solid #ccc";
  
    emailError.textContent = "";
    document.getElementById("email").style.border = "1px solid #ccc";
  
    phoneError.textContent = "";
    document.getElementById("phone").style.border = "1px solid #ccc";
  
    if (name.trim() === "") {
      nameError.textContent = "This field is required.";
      document.getElementById("name").style.border = "2px solid red";
      return;
    }
  
    if (email.trim() === "") {
      emailError.textContent = "This field is required.";
      document.getElementById("email").style.border = "2px solid red";
      return;
    }
  
    if (phone.trim() === "") {
      phoneError.textContent = "This field is required.";
      document.getElementById("phone").style.border = "2px solid red";
      return;
    }
  
    const nameRegex = /^[A-Za-z\s]+$/;
    if (!nameRegex.test(name)) {
      nameError.textContent = "Name should contain only letters and spaces.";
      document.getElementById("name").style.border = "2px solid red";
      return;
    }
  
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!emailRegex.test(email)) {
      emailError.textContent = "Please enter a valid email address.";
      document.getElementById("email").style.border = "2px solid red";
      return;
    }
  
    const phoneRegex = /^\+?\d{7,12}$/;
    if (!phoneRegex.test(phone)) {
        phoneError.textContent = "Please enter a valid phone number";
        document.getElementById("phone").style.border = "2px solid red";
        return;
    }

  
    window.location.href = "./html/step2.html";
}
   
const data_monthly = [9, 13, 15];
const data_yearly = [90, 130, 150]


let plan = [];



const checkbox = document.getElementById("slide");
const cards = document.querySelectorAll('.card');


function onToggle_mo_yr() {
    if (checkbox) {
        checkbox.addEventListener("click", function () {
            
            const isMonthly = !checkbox.checked;
            const heightStyle = isMonthly ? "67%" : "79%";

            !isMonthly ? plan = ["Arcade", 90, "yearly"] : plan = ["Arcade", 9, "monthly"];

            cards.forEach((card, index) => {
                card.style.height = heightStyle;

                const img = card.querySelector('.card-img');
                img.style.height = isMonthly ? "70%" : "60%";

                const cardDetail = card.querySelector('.card-detail');
                cardDetail.style.height = isMonthly ? "29%" : "40%";


                const priceText = isMonthly ? `$${data_monthly[index]}/mo` : `$${data_yearly[index]}/yr`;

                const pElement = cardDetail.querySelector('p');
                pElement.innerHTML = priceText;

                if (!isMonthly) {
                    const hfree = document.createElement('h6');
                    hfree.innerHTML = "2 Months free";
                    card.querySelector('.card-detail').appendChild(hfree);

                }
                else {
                    const h6Element = cardDetail.querySelector('h6');
                    cardDetail.removeChild(h6Element);
                }
                localStorage.setItem("selectedPlan", JSON.stringify(plan));
            });

        });

    }
}


onToggle_mo_yr();


function cardSelect() {
    let selectedCard = cards[0];
    if (selectedCard) selectedCard.classList.add('clicked');

    cards.forEach((card, index) => {

        card.addEventListener("click", function () {
            
            selectedCard.classList.remove('clicked');
            card.classList.add('clicked');
            selectedCard = card;


            const curr_plan = card.querySelector('.card-detail h2').innerHTML;
            plan[0] = curr_plan;

            if (!checkbox.checked) {
                plan[1] = data_monthly[index];
                plan[2] = "monthly";
            }
            else {
                plan[1] = data_yearly[index];
                plan[2] = "yearly";
            }

            localStorage.setItem("selectedPlan", JSON.stringify(plan));
        });

    });

}

cardSelect();
// third page 


const retrievedPlan = localStorage.getItem("selectedPlan");
if (retrievedPlan) {
    plan = JSON.parse(retrievedPlan);
}


function backButtonData() {

    document.querySelectorAll(".back").forEach(function (backlink) {
        backlink.addEventListener("click", function (event) {


            localStorage.removeItem("add-on");

            add_on_data = [];
            localStorage.setItem("add-on", JSON.stringify(add_on_data));
        });
    });

    const back2 = document.querySelector("#back2");

    if (back2) {
        back2.addEventListener("click", function () {

            localStorage.removeItem("selectedPlan");

            plan = ["Arcade", 9, "monthly"];
            localStorage.setItem("selectedPlan", JSON.stringify(plan));
        });
    }

}

backButtonData();



// console.log(plan);

function addOnData() {

    const addOn = document.querySelectorAll(".add-on");

    const year_price = [10, 20, 20];
    const month_price = [1, 2, 2];
    let add_on_data = [];
    addOn.forEach((add, index) => {

        const para = add.querySelector(".cost p");

        if (plan[2] === "yearly") {
            para.innerHTML = `+${year_price[index]}/yr`;
        }
        else {
            para.innerHTML = `+${month_price[index]}/mo`;
        }


        const inp = add.querySelector("#toggle-pick");
        const add_on = add.querySelector(".h3-p h3");

        inp.addEventListener("change", function () {

            if (inp.checked) {

                add.style.backgroundColor = "hsl(217, 100%, 97%)";
                add.style.borderColor = "hsl(243, 100%, 62%)";

                add_on_data.push(add_on.innerHTML);

                if (plan[2] === "yearly") {
                    add_on_data.push(year_price[index]);
                }
                else {
                    add_on_data.push(month_price[index]);
                }

            } else {

                add.style.backgroundColor = "";
                add.style.borderColor = "";

                add_on_data.pop();
                add_on_data.pop();

            }

            localStorage.setItem("add-on", JSON.stringify(add_on_data));
        });

    });
}

addOnData();

//step 4
const retrievedadd = localStorage.getItem("add-on");
if (retrievedadd) {

    add_on_data = JSON.parse(retrievedadd);
}


function checkOutPage() {
    const costList = document.querySelector(".cost-list");

    const finishDiv = document.createElement("div");
    finishDiv.className = "finish";

    costList.appendChild(finishDiv);

    let money = 0;

    const finalPlanDiv = document.createElement("div");
    finalPlanDiv.className = "final-plan";

    const planTitle = document.createElement("h3");
    planTitle.textContent = `${plan[0]} (${plan[2]})`;

    const planChange = document.createElement("p");
    planChange.textContent = "Change";
    planChange.id = "change";

    planChange.addEventListener("click", function () {

        localStorage.removeItem("selectedPlan");

        plan = ["Arcade", 9, "monthly"];
        add_on_data = [];
        localStorage.setItem("selectedPlan", JSON.stringify(plan));
        localStorage.setItem("add-on", JSON.stringify(add_on_data));
        window.location.href = './step2.html';
    })


    const planPrice = document.createElement("p");
    planPrice.textContent = `$${plan[1]}` + (plan[2] === "yearly" ? "/yr" : "/mo");
    planPrice.id = "planPrice";

    money += plan[1];


    finalPlanDiv.appendChild(planTitle);
    finalPlanDiv.appendChild(planChange);
    finishDiv.appendChild(finalPlanDiv);
    finishDiv.appendChild(planPrice);


    const hr = document.createElement("hr");
    costList.appendChild(hr)

    const planBill = document.createElement("div");
    planBill.className = "plan-bill";

    for (let i = 0; i < add_on_data.length; i += 2) {
        const div1 = document.createElement("div");
        div1.className = "add-money";

        const p1 = document.createElement("p");
        p1.textContent = add_on_data[i];
        div1.appendChild(p1);

        const p2 = document.createElement("p");
        p2.textContent = `$${add_on_data[i + 1]}` + (plan[2] === "yearly" ? "/yr" : "/mo");
        div1.appendChild(p2);

        money += add_on_data[i + 1];

        planBill.appendChild(div1);
    }



    costList.appendChild(planBill);

    const billing = document.createElement("div");
    billing.className = "billing";

    const dd = document.createElement("div");
    dd.className = "total-money";

    const p_total = document.createElement("p");
    p_total.innerHTML = `Total ${"(per " + (plan[2] === "yearly" ? "year)" : "month)")}`
    p_total.id = "tot";


    const p_money = document.createElement("p");
    p_money.innerHTML = `+$${money}` + (plan[2] === "yearly" ? "/yr" : "/mo");
    p_money.id = "total-money"


    dd.appendChild(p_total);
    dd.appendChild(p_money);
    billing.appendChild(dd);

    costList.appendChild(billing);
}

checkOutPage();